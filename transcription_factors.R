#Sys.setenv(KERAS_IMPLEMENTATION = "tensorflow")
#Sys.setenv(KERAS_BACKEND="tensorflow")
library(keras)
#library(tidyverse)
library(dplyr)
library(pROC)
#library(hashmap)
#setwd('/home/rafal/Desktop/Praca Dyplomowa/R_pbm_keras')
setwd('/home/r.kowalczyk')

# load full dataset

#training_dataset <- read.csv2('DREAM5_PBM_Data_Needed_For_Predictions.txt', sep="")
#testing_dataset <- read.csv2('Answers.txt', sep="")

setwd('/home/r.kowalczyk')
load("dream_dataset.RDA")

#save(dream_dataset, file = 'dream_dataset_for_prediction.RDA')
#load('dream_dataset.RDA')




TF_Ids <- training_dataset %>%
  group_by(TF_Id)%>%
  summarise()

#TF_Ids <- TF_Ids[1:66,]
#x <- TF_Ids[1,]
#x <- 'TF_1'

final_score <- data.frame()

for(k in 1:66){
  
  x<-TF_Ids$TF_Id[k]
  
  TF_train_dataset <- training_dataset %>%
    filter(TF_Id == x)%>%
    mutate(Signal_Mean=as.character(Signal_Mean))%>%
    mutate(Signal_Mean=as.numeric(Signal_Mean))
  
  TF_test_dataset <- testing_dataset %>%
    filter(Id == x) %>%
    mutate(TF_Id = Id) %>%
    mutate(Signal_Mean=as.character(Answer))%>%
    mutate(Signal_Mean=as.numeric(Signal_Mean))
  
  
  # split sequences into single characters
  sequence_split_train <- strsplit(as.character(TF_train_dataset$Sequence), "")
  sequence_split_test <- strsplit(as.character(TF_test_dataset$Sequence), "")
  
  # transform list to data frame
  sequence_split_train_df <- data.frame(matrix(unlist(sequence_split_train), nrow = length(sequence_split_train), byrow = TRUE), stringsAsFactors = FALSE)
  sequence_split_test_df <- data.frame(matrix(unlist(sequence_split_test), nrow = length(sequence_split_test), byrow = TRUE), stringsAsFactors = FALSE)
  
  # one-hot encoding
  dict <- list('A' = 1, 'C' = 2, 'G' = 3, 'T' = 4 )
  
  x_train <- sequence_split_train_df %>%
    mutate_all(funs(dict[.]))%>%
    data.matrix()
  x_test <- sequence_split_test_df %>%
    mutate_all(funs(dict[.]))%>%
    data.matrix()
  y_train <- as.numeric(TF_train_dataset$Signal_Mean)
  y_test <- as.numeric(TF_test_dataset$Signal_Mean)
  
  #input<- layer_input (shape = c(60,4))
  
  input<- layer_input (shape = 60)
  
  emb <- layer_embedding(input, 4, 4 )
  
  prenet_out <- layer_dense(emb, units = 512, input_shape = c(60, 4), activation = 'relu')%>%
    layer_dropout(0.2)%>%
    layer_dense(units = 256, activation = 'relu')%>%
    layer_dropout(0.2)
  
  ## conv 1D bank
  
  #output_layer <- layer_conv_1d(model, filter = 256, padding = 'same', kernel_size = 1)
  output_layer <- layer_conv_1d(prenet_out, filter = 256, padding = 'same', kernel_size = 1)
  
  for(i in 2:16){
    
    temp_layer <- layer_conv_1d(prenet_out, filter = 256, padding = 'same', kernel_size = i)
    output_layer <- layer_concatenate(list(output_layer, temp_layer))
    
  }
  
  model <- layer_activation(output_layer,'relu')
  
  #conv_projection_model <- output_layer %>%
  model <- model %>%
    #model <- prenet_out %>%
    layer_conv_1d(filter = 256, padding = 'same', kernel_size = 3 )%>%
    layer_activation('relu')%>%
    layer_conv_1d(filter = 256, padding = 'same', kernel_size = 3 )%>%
    layer_activation('relu')
  
  #residual conncetion
  model <- layer_add(list(model, prenet_out))
  
  #highway net
  
  #nput_tensor <- final_model
  
  for (i in 1:4){
    gate <- layer_dense(model, units = 256, activation = 'relu')
    negated_gate <- layer_lambda( gate ,function(x) { return (1.0 - x)} )
    transformed <- layer_dense(model, units = 256, activation = 'relu')
    
    transformed_gated <- layer_multiply(list(gate, transformed))
    identity_gated <- layer_multiply(list(negated_gate, model))
    input_tensor <- layer_add(list(transformed_gated, identity_gated))
    #model <- input_tensor
  }
  
  
  
  #model <- model %>%
  #bidirectional(layer_cudnn_gru(units = 16))
  #  layer_cudnn_lstm(128)
  #end_model <- input_tensor%>%
  #layer_flatten()%>%
  #  layer_dense(1)
  
  #odel <-output_layer%>%
  model <- model%>%
    layer_flatten()%>%
    layer_dense(1)
  
  
  
  #mod <- keras_model(input, end_model)
  mod <- keras_model(input, model)
  
  opt <- optimizer_adam(lr = 0.0005)
  #opt <- optimizer_adam(lr = 0.05)
  mod %>%
    compile(loss = 'mean_squared_error', optimizer = opt)
  
  hist <- mod %>% fit(
    x_train, y_train,
    epochs = 20,
    batch_size = 32,
    validation_data = list(x_train, y_train),
    shuffle = TRUE
  )
  
  prediction <- mod %>%
    predict(x_test)
  
  
  prediction <- as.data.frame(prediction)
  output<-data.frame(y_test, prediction)
  score <- cor(output, method = 'pearson')
  pearson_score <- score[2,1]
  
  colnames(output) <- c('y_test', 'prediction')
  
  output_ext <- output %>%
    mutate(is_positive_test = ifelse(y_test >= (mean(output$y_test) + 4 * sd(output$y_test)), 1 , 0),
           is_positive_prediction = ifelse(prediction >= (mean(output$prediction) + 4 * sd(output$prediction)), 1, 0))
  
  output_ext <- output %>%
    data.frame()%>%
    arrange(desc(y_test))%>%
    group_by()%>%
    mutate(row_number = row_number(),
           is_positive_test = ifelse(row_number() < 50 , 1 , 0))
  
  #auc(output_ext$is_positive_test, output_ext$prediction, plot = FALSE)
  
  roc_obj <- roc(output_ext$is_positive_test, output_ext$prediction)
  
  auc_score <- as.numeric(auc(roc_obj))
  
  final_score <- final_score %>%
    bind_rows( data.frame(x, pearson_score, auc_score))
  
  load("TF_scores_all.RDA")
  TF_scores_all <- TF_scores_all %>%
    bind_rows(data.frame(x, pearson_score, auc_score))
  save("TF_scores_all", file = "TF_scores_all.RDA")
  
}

#load("TF_scores_all.RDA")
#save(TF_scores_all, file = "TF_scores_all.RDA")

